#include <Arduino.h>
#include <driver/adc.h>

#include "FastLED.h"

#include "Wire.h"
#include <U8g2lib.h>

// U8G2_SSD1306_128X64_VCOMH0_1_SW_I2C
// U8G2_SSD1306_128X64_VCOMH0_2_SW_I2C
U8G2_SSD1306_128X64_NONAME_F_SW_I2C
u8g2(U8G2_R0, /* clock=*/SCL, /* data=*/SDA, /* reset=*/U8X8_PIN_NONE); // All Boards without Reset of the Display
                                                                        //
static const int RGBLedPin = 23;

static const int NUM_LEDS = 3;
CRGB leds[NUM_LEDS];

static const int lightPins[] = {32, 25, 26, 27, 17, 16};
static const int numberOfPins = sizeof(lightPins) / sizeof(lightPins[0]);

void setup() {
    Serial.begin(115200);
    delay(500);

    // init motors
    sigmaDeltaSetup(0, 312500);
    sigmaDeltaSetup(1, 312500);
    sigmaDeltaAttachPin(18, 0);
    sigmaDeltaAttachPin(19, 1);
    sigmaDeltaWrite(0, 0);
    sigmaDeltaWrite(1, 0);

    // init RGB-LEDs
    FastLED.addLeds<NEOPIXEL, RGBLedPin>(leds, NUM_LEDS);
    FastLED.clear();
    leds[0] = CRGB::Black;
    leds[1].setRGB(0, 0, 0);
    // leds[2].setRGB(0, 0, 0);
    leds[2].setHSV(127, 255, 63);
    FastLED.show();

    // init sensors
    adc1_config_width(ADC_WIDTH_BIT_12);
    adc1_config_channel_atten(ADC1_CHANNEL_7, ADC_ATTEN_DB_0);
    for (int i = 0; i < numberOfPins; ++i) {
      pinMode(lightPins[5], OUTPUT);
      digitalWrite(lightPins[i], LOW);
      pinMode(lightPins[i], INPUT_PULLDOWN);
    }

    // init display
    u8g2.begin();
    u8g2.setContrast(127);
    u8g2.setFont(u8g2_font_courB24_tn);
}

void loop() {
    /* read and print front sensor*/
    pinMode(lightPins[5], OUTPUT);
    digitalWrite(lightPins[5], HIGH);
    delay(1);
    int frontsensor = analogRead(35);
    digitalWrite(lightPins[5], LOW);
    pinMode(lightPins[5], INPUT_PULLDOWN);

    /* print in console */
    Serial.printf("Frontsensor: %d\n", frontsensor);


    /* run motors */
    // sigmaDeltaWrite(0, 100);
    // sigmaDeltaWrite(1, 100);

    /* show sensor in display */
    // char cstr[6];
    // strcpy(cstr, u8x8_u8toa(frontsensor, 4));
    // u8g2.firstPage();
    // do {
    //     u8g2.drawStr(0, 28, cstr);
    // } while (u8g2.nextPage());
    

    delay(100); // don't run main loop so fast
}
