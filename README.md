# Dezibot HandsOn

## Installation

It's recommanded to use the [PlatformIO IDE](https://platformio.org/install/ide?install=vscode). It takes care of compiling, uploading, dependencies, tests, etc. and you have a modern and extendible editor. Not this 90s like Arduino IDE. But if you are experinced with arduino, feel free to use your own environment.
If you doing it for yourself, you need the following libraries to run the project: `U8g2` for the display and `FastLED` for the rgb-leds. For the PlatformIO users is nothing to do. Everthing is provided throug the `platformio.ini` inside the repo.

## Get the source

Clone this repo with

    git clone git@git.informatik.uni-leipzig.de:jagla/dezibot-handson.git

## Run it

Open the cloned project with VS-Code (or your custom setting).
The connect the Dezibot with a micro-USB cable to your machine. Turn the Dezibot on.
Check, if the dezibot is shown as device

    ls /dev

If something like `/dev/ttyUSB*` appears, the dezibot is succesfull connected to your system.
Upload the code to the bot either via VS-Code by pressing the leftarrow-button in the bottom menu or via terminal (while inside the root folder of the project)

    pio run -t upload

Enjoy!
